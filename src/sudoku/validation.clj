(ns sudoku.validation
  (:require [sudoku.board :refer [get-row get-column get-square]]
            [sudoku.board :as brd]
            [sudoku.util :as util]
            [sudoku.validation :as vld]))

(defn contains-all-numbers? [coll]
  (every? true?
          (map #(some (partial = %) coll) (range 1 10))))

(defn repeats? [coll]
  (let [filtered-coll (filter (complement nil?) coll)]
    (not= (count filtered-coll)
          (count (set filtered-coll)))))

(defn set-is-valid? [coll]
  (and (contains-all-numbers? coll)
       (not (repeats? coll))))

(defn cell-is-valid? [board r c]
  (and (set-is-valid? (get-row board r))
       (set-is-valid? (get-column board c))
       (set-is-valid? (flatten (get-square board r c)))))

(defn board-is-valid? [board]
  (and
   ;; validate every row
   (every? true?
           (map set-is-valid? board))
   ;; validate every column
   (every? true?
           (map set-is-valid? (map (partial get-column board) (range 9))))
   ;; validate every square
   (every? true?
           (map set-is-valid?
                (for [r (map (partial * 3) (range 3))
                      c (map (partial * 3) (range 3))]
                  (flatten (get-square board r c)))))))

(defn cell-violates-rules? [board r c]
  (let [row (get-row board r)
        col (get-column board c)
        square (flatten (get-square board r c))
        value (brd/get-cell-contents board r c)]
    (or (some (partial = value) (util/get-duplicates row))
        (some (partial = value) (util/get-duplicates col))
        (some (partial = value) (util/get-duplicates square)))))
