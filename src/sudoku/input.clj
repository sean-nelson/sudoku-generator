(ns sudoku.input
  (:require [quil.core :as q]
            [sudoku.board :as brd]
            [sudoku.conf :as conf]
            [sudoku.notes :as note]
            [sudoku.render :as rnd]
            [sudoku.util :as util]))

(defn get-cell-coords [x y]
  (let [[board-width board-height] (rnd/get-board-size)
        board-x (- x conf/margin)
        board-y (- y conf/margin)
        cell-size (/ board-width 9)]
    [(int (Math/floor (/ board-y cell-size)))
     (int (Math/floor (/ board-x cell-size)))]))

(defn bounded [f a b]
  (util/clamp (f a b) 0 8))

(defn handle-selected-movement [{[r c] :selected :as state} key]
  (assoc state :selected
         (if-not (nil? r)
           (case key
             (:left :a :h) [r (bounded - c 1)]
             (:right :d :l) [r (bounded + c 1)]
             (:up :w :k) [(bounded - r 1) c]
             (:down :s :j) [(bounded + r 1) c]
             [r c])
           (case key
             (:left :right :up :down :w :a :s :d :h :j :k :l) [0 0]
             nil))))

(defn handle-number-press [{board :board
                            [r c] :selected
                            select-mode :select-mode
                            notes :notes
                            :as state} key]
  (let [num (case key
              :1 1
              :2 2
              :3 3
              :4 4
              :5 5
              :6 6
              :7 7
              :8 8
              :9 9
              nil)]
    (if (nil? num)
      state
      (case select-mode
        :fill
        (-> state
            (assoc :board
                   (if-not (nil? r)
                     (brd/replace-cell board r c num)
                     board))
            (assoc :notes
                   (-> notes
                       (note/clear-notes r c)
                       (note/remove-connected-notes r c num))))
        :note
        (assoc state :notes (note/update-notes notes r c num))))))

(defn handle-select-mode-keys [{select-mode :select-mode :as state} key]
  (assoc state :select-mode
         (if-not (= key :space)
           select-mode
           (if (= select-mode :fill)
             :note
             :fill))))

(defn handle-backspace [{[r c] :selected notes :notes board :board :as state} key-code]
  (if-not (= key-code 8)
    state
    (-> state
        (assoc :board (brd/replace-cell board r c nil))
        (assoc :notes (note/clear-notes notes r c)))))

(defn mouse-pressed [{board :board :as state} {x :x y :y :as event}]
  (let [[r c] (get-cell-coords x y)]
    (q/redraw)
    (assoc state :selected [r c])))

(defn key-pressed [{[r c] :selected board :board select-mode :select-mode :as state} {key :key key-code :key-code :as _event}]
  (let [new-state
        (-> state
            (handle-selected-movement key)
            (handle-number-press key)
            (handle-select-mode-keys key)
            (handle-backspace key-code))]

    (q/redraw)
    ; new-state is calculated inside a let so that redraw can be called as late in this function as possible
    ; This helps with an issue where the board sometimes does not redraw itself
    new-state))
