(ns sudoku.core
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [sudoku.conf :as conf]
            [sudoku.generate :as gen]
            [sudoku.input :as in]
            [sudoku.render :as rnd]
            [sudoku.validation :as vld]))

(defn setup []
  ; setup function returns initial state. It contains
  ; circle color and position.
  (q/no-loop)
  {:board (gen/generate-unsolved-board)
   :selected nil
   ; can be one of :fill, :note
   :select-mode :fill
   ; vector of maps like {:coord [r c]
   ;                      :nums [1 2 3 4]}
   :notes []
   ; one of :playing :complete
   :game-stage :playing})

(defn update-state [{board :board game-stage :game-stage :as state}]
  (if (and (= game-stage :playing)
           (vld/board-is-valid? board))
    (assoc state :game-stage :complete)
    state))

(q/defsketch sudoku
  :title "Sudoku"
  :size conf/screen-size
  ; setup function called only once, during sketch initialization.
  :setup setup
  :update update-state
  :draw rnd/draw-state
  :mouse-pressed in/mouse-pressed
  :key-pressed in/key-pressed
  :features [:keep-on-top]
  ; This sketch uses functional-mode middleware.
  ; Check quil wiki for more info about middlewares and particularly
  ; fun-mode.
  :middleware [m/fun-mode])
