(ns sudoku.board)

;; example board
(comment [[1 2 3 4 5 6 7 8 9]
          [4 5 6 7 8 9 1 2 3]
          [7 8 9 1 2 3 4 5 6]
          [2 3 4 5 6 7 8 9 1]
          [5 6 7 8 9 1 2 3 4]
          [8 9 1 2 3 4 5 6 7]
          [3 4 5 6 7 8 9 1 2]
          [6 7 8 9 1 2 3 4 5]
          [9 1 2 3 4 5 6 7 8]])

(defn empty-board [] (into [] (repeat 9 (into [] (repeat 9 nil)))))

(defn print-board [board]
  (map println board))

(defn get-row [board n]
  (nth board n))

(defn get-column [board n]
  (map #(nth % n) board))

(defn get-square [board r c]
  (let [start-row (* 3 (Math/floorDiv r 3))
        start-col (* 3 (Math/floorDiv c 3))]
    [(subvec (nth board start-row) start-col (+ start-col 3))
     (subvec (nth board (+ 1 start-row)) start-col (+ start-col 3))
     (subvec (nth board (+ 2 start-row)) start-col (+ start-col 3))]))

(defn get-intersecting-numbers [board r c]
  (into #{} (filter (complement nil?) (into (flatten (get-square board r c))
                                            (into (get-row board r)
                                                  (get-column board c))))))

(defn get-all-cell-coords []
  (for [r (range 9)
        c (range 9)]
    [r c]))

(defn get-row-coords [r]
  (for [r (list r)
        c (range 9)]
    [r c]))

(defn get-column-coords [c]
  (for [r (range 9)
        c (list c)]
    [r c]))

(defn get-square-coords [r c]
  (let [start-row (* 3 (Math/floorDiv r 3))
        start-col (* 3 (Math/floorDiv c 3))]
    (for [r (map (partial + start-row) (range 3))
          c (map (partial + start-col) (range 3))]
      [r c])))

(defn get-all-connected-coords [r c]
  (into (get-square-coords r c)
        (into (get-row-coords r)
              (get-column-coords c))))

(defn replace-cell [board r c val]
  (assoc board r (assoc (get-row board r) c val)))

(defn random-not-in-set
  ([coll] (random-not-in-set coll 9))
  ([coll max-int]
   (if (= max-int (count coll))
     nil
     (loop [int (+ 1 (rand-int max-int))]
       (if (nil? (some (conj #{} int) coll))
         int
         (recur (+ 1 (rand-int max-int))))))))

;; TODO this should be in generate
(defn fill-cell [board r c]
  (assoc board r (assoc (get-row board r) c (random-not-in-set (get-intersecting-numbers board r c)))))

(defn get-cell-contents [board r c]
  (nth (nth board r) c))
