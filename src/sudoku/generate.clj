(ns sudoku.generate
  (:require [sudoku.validation :as vld]
            [sudoku.board :as brd]
            [sudoku.conf :as conf]))


(defn generate-solved-board []
  (loop []
    (let [potential-board
          (reduce (fn [board [r c]]
                    (brd/fill-cell board r c)) (brd/empty-board)
                  (brd/get-all-cell-coords))]
      (if (vld/board-is-valid? potential-board)
        potential-board
        (recur)))))

(defn generate-unsolved-board []
  (reduce (fn [board [r c]]
            (brd/replace-cell board r c nil))
          (generate-solved-board)
          (vals (select-keys (vec (brd/get-all-cell-coords))
                             (reduce (fn [coll _] (conj coll (brd/random-not-in-set coll 81))) [] (repeat conf/filled-cells nil))))))
