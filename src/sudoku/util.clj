(ns sudoku.util)

(defn range-partitions [n partitions]
  (map (partial * (/ n partitions)) (range (+ 1 partitions))))

(defn get-duplicates [coll]
  (for [[val count] (frequencies coll)
        :when (and (not= nil val)
                   (> count 1))]
    val))

(defn clamp
  "Given integer `n` return the closest value in the range `low`-`high` (inclusive)"
  [n low high]
  (cond
    (< n low) low
    (> n high) high
    :else n))
