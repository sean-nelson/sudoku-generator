(ns sudoku.notes
  (:require [sudoku.board :as brd]))

(defn get-cell-notes [notes r c]
  (some (fn [{coord :coord :as note}]
          (when (= [r c] coord)
            note)) notes))

(defn update-notes
  "Given the notes vector `notes` from the state. Coordinates `r` and `c` and a number `val`.
  Returns the notes vector, updating the entry for [`r` `c`] by adding or removing `val`"
  [notes r c val]
  ; get notes for this cell
  (let [{nums :nums :as note} (get-cell-notes notes r c)
        other-notes (filter (fn [{coord :coord}] (not= [r c] coord)) notes)]
    (conj other-notes
          (if (nil? nums)
            {:coord [r c]
             :nums [val]}
            ; if val is already in nums
            (if (some (partial = val) nums)
              ; remove val from nums
              (assoc note :nums (vec (filter (partial not= val) nums)))
              ; otherwise add it to nums
              (assoc note :nums (conj nums val)))))))

(defn clear-notes [notes r c]
  (filter #(not= [r c] (:coord %)) notes))

(defn remove-note
  "Removes `val` from the nums of coordinate [`r` `c`] if it exists"
  [notes r c val]
  (let [{nums :nums :as note} (get-cell-notes notes r c)
        other-notes (filter (fn [{coord :coord}] (not= [r c] coord)) notes)]
    (conj other-notes
          (assoc note :nums (filter (partial not= val) nums)))))

(defn remove-connected-notes
  "Given coordinates [`r` `c`] and `val` clear all notes from cells which touch [`r` `c`] via sudoku rules"
  [notes r c val]
  (reduce (fn [notes [r c]]
            (remove-note notes r c val)) notes (brd/get-all-connected-coords r c)))
