(ns sudoku.render
  (:require [quil.core :as q]
            [sudoku.board :as brd]
            [sudoku.conf :as conf]
            [sudoku.notes :as note]
            [sudoku.util :as util]
            [sudoku.validation :as vld]))

(defn get-board-size []
  (let [[screen-width screen-height] conf/screen-size]
    [(- screen-width (* 2 conf/margin))
     (- screen-height (* 2 conf/margin))]))

(defn draw-grid [width height size]
  (let [coord-pairs (->> (into
                          ;; vertical lines
                          (for [x (util/range-partitions width size)
                                y (list 0 height)]
                            [x y])
                          ;; horizontal lines
                          (for [y (util/range-partitions height size)
                                x (list 0 width)]
                            [x y]))
                         (partition 2)
                         (map flatten))]
    (doseq [pair coord-pairs]
      (apply q/line pair))))

(defn draw-note [num]
  (q/fill 0)
  (q/text-font (q/create-font "Courier New" 18))
  (let [[board-width board-height] (get-board-size)
        cell-size (/ board-width 9)
        i (- num 1)
        x (+ 7 (* (mod i 3) (/ cell-size 3)))
        y (+ 20 (* (Math/floor (/ i 3)) (/ cell-size 3)))]
    (q/text (str num)
            x y)))

(defn cell-contains?
  "Returns true if cell at coordinate [`r` `c`] contains `val` as its content or as a note"
  [{board :board notes :notes :as _state} r c val]
  (let [nums (some (fn [{coord :coord nums :nums}]
                     (when (= coord [r c])
                      nums)) notes)]
    (if (nil? val)
      false
      (or (= val (brd/get-cell-contents board r c))
          (some (partial = val) nums)))))

(defn draw-cell-contents [{board :board selected :selected select-mode :select-mode :as state} r c]
  (let [[board-width board-height] (get-board-size)
        cell-x (* c (/ board-width 9))
        cell-y (* r (/ board-height 9))
        cell-size (/ board-width 9)
        cell-content (brd/get-cell-contents board r c)
        note-nums (:nums (note/get-cell-notes (:notes state) r c))
        selected-val (when-not (nil? selected)
                       (brd/get-cell-contents board (first selected) (second selected)))]
    (q/with-translation [cell-x cell-y]
      ; draw cell background fill
      (let [selected? (= selected [r c])
            invalid? (vld/cell-violates-rules? board r c)
            cell-contains-selected? (cell-contains? state r c selected-val)
            draw? (or selected? invalid? cell-contains-selected?)]
        (when invalid?
          (q/fill [255 0 0 150]))
        (when (and cell-contains-selected?
                   (not (nil? selected-val)))
          (q/fill [150 0 150 50]))
        (when selected?
          (apply q/fill
                 (case select-mode
                   :fill [0 255 0 150]
                   :note [150 0 150 150]
                   [255 255 255 255])))
        (when draw?
          (q/stroke-weight 0)
          (q/rect 0 0 cell-size cell-size)))
      ; draw cell content
      (when-not (nil? cell-content)
        (if (vld/cell-violates-rules? board r c)
          (q/fill 255 0 0)
          (q/fill 0))
        (q/text-font (q/create-font "Courier New" 38))
        (q/text (str cell-content)
                (- (/ cell-size 2) 12)
                (+ (/ cell-size 2) 14)))
      ; draw notes
      (when (and (nil? cell-content)
                 (not (empty? note-nums)))
        (doseq [num note-nums]
          (draw-note num))))))

(defn draw-win-screen []
  (let [[board-width board-height] (get-board-size)]
    (q/fill 0)
    (q/text-font (q/create-font "Courier New" 38))
    (q/text "You win!" (- (/ board-width 2) 50) (/ board-height 2))))

(defn draw-state [{board :board selected :selected game-stage :game-stage :as state}]
  ; Clear the sketch by filling it with background color.
  (q/background 255)
  (if (= :complete game-stage)
    (draw-win-screen)
    (let [[board-width board-height] (get-board-size)
          cell-size (/ board-width 9)]
      (q/with-translation [conf/margin conf/margin]
        ;; draw cells
        (q/stroke 0)
        (q/stroke-weight 1)
        (draw-grid board-width board-height 9)
        ;; draw squares
        (q/stroke 0)
        (q/stroke-weight 3)
        (draw-grid board-width board-height 3)
        (doseq [r (range 9)
                c (range 9)]
          (draw-cell-contents state r c))))))
